#!/bin/bash
# Installs and sets up everything on a new system

# Fedora packet manager
# Information mostly from here: https://fedoramagazine.org/wsl-fedora-33/
update="dnf update" # We could use dnf -y update to let everything install silenty
install="dnf install"
fedora_packages="sudo"

general_packages="curl passwd gcc gcc-c++ fd-find ripgrep nodejs python openssh git neovim tmux util-linux-user zsh"

# When the WSL image is first opened we are logged in as the root user.
echo "Updating system"
$update

echo "Installing packages as root"
$install $fedora_packages $general_packages

echo "Setting up the artqmo user account "
useradd -G wheel artqmo
passwd artqmo

echo "Making the user the default for next start up."
cat > /etc/wsl.conf << ENDOFFILE
[user]
default=artqmo
ENDOFFILE

echo "Swichting to created user to finish installation"
su artqmo
cd ~

whoami
pwd
