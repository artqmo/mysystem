echo "Start bootstrapping by installing minimal required packages"
# sudp apt update
# sudo apt upgrade
# sudo apt install ansible git
sudo zypper install -y ansible git
echo "Create bootstrap dir in home and download ansible tasks"
mkdir ~/bootstrap
cd ~/bootstrap
git clone --depth 1 https://gitlab.com/artqmo/mysystem.git
