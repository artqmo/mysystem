# How to

## In Windows
1. Install scoop
2. Install the following packages:
	- alacritty
	- win32yank (For clipboard support in neovim)
	- FiraMono Nerd Font (A nice terminal font with symbols)
3. Config of alacritty in `...\AppData\Roaming\alacritty\alacritty.toml`
```
[font.normal]
family = "FiraMono Nerd Font"

[[keyboard.bindings]]
chars = "\u0000"

key = "Space"
mods = "Control"

[shell]
args = ["--cd ~"]
program = 'C:\Windows\System32\wsl.exe'


[window.padding]
x = 0
y = 0
```



## In OpenSuse Tumbleweed
1. Copy the content into a newly created file called bs.sh
in your home directory.
```bash
echo "Start bootstrapping by installing minimal required packages"
sudo zypper install -y ansible git
echo "Create bootstrap dir in home and download ansible tasks"
mkdir ~/bootstrap
cd ~/bootstrap
git clone --depth 1 https://gitlab.com/artqmo/mysystem.git
```
2. Run the content with: source bs.sh
3. Excecute the ansible config script with:
```bash
ansible-playbook mysystem/ansible/main.yml --ask-become-pass
```

## Use proxy for package manager (zypper) in Tumbleweed
1. Add the proxy under /etc/sysconfig/proxy for global use or for a single user
add the proxy into .zshrc.
`export http_proxy="xxx.xxx.xxx:xxx"`
2. In global settings the proxy can be activated/deactivated with the PROXY_ENABLED flag.
3. More information can be found [here](https://www.suse.com/de-de/support/kb/doc/?id=000017441)

