#!/bin/bash

echo "After the initial system setup we are now logged in as artqmo"

echo "Configuring git"
git config --global user.email "8667453-artqmo@users.noreply.gitlab.com"
git config --global user.name "artqmo"
  

echo "Now we create an SSH key that we can use for GitLab"
# https://docs.gitlab.com/ee/user/ssh.html
ssh-keygen -t ed25519 -C "$(date +"%Y%m%d") WSL" -N '' <<<$'\n'

FILE=$(<~/.ssh/ed25519.pub)
echo $FILE

echo "Please add the public key to your GitLab profile"
echo "https://gitlab.com/-/profile/keys"
read -p "After you did this write anything and confirm to continue: " dummy

# echo "Now we clone our configuration and system setup repos"
# cd ~
# git clone git@gitlab.com:artqmo/myconfig.git

# https://github.com/savq/paq-nvim
echo "We install neovim plugin manager"
git clone --depth=1 https://github.com/savq/paq-nvim.git "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/pack/paqs/start/paq-nvim

echo "Install and link dotfiles"
cd ~
git clone git@gitlab.com:artqmo/mysystem.git
git clone git@gitlab.com:artqmo/dotfiles.git
bash dotfiles/set_up_config_in_linux.sh

# echo "Configure fish shell and make it default"
# fish
# curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
# fisher install IlanCosman/tide@v5
# chsh -s /usr/bin/fish

echo "Configure zsh shell and make it default"
zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $ZSH_CUSTOM/themes/powerlevel10k
git clone --depth=1 https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone --depth=1 https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
source ~/.zshrc
chsh -s /usr/bin/zsh

