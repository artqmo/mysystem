
echo "Install reveal.js for presentations"
mkdir presentations
cd presentations
git clone --depth=1 https://github.com/hakimel/reveal.js.git
cd reveal.js && npm install

cd ~
